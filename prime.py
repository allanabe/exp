import math

# Test by trying a float-conversion of user-input
# If float() works, return the float
# If float() fails, return the string 'Nan' 
def isNumber(userArg):
    while True:
        try:
            return int(userArg)
            break
        except ValueError: # If float() fails a "ValueError" exception is raised (right type but inappropriate value)
            return 'NaN'

num = 0

while num != 'x':
    # print()
    num = input('Enter an integer (x to exit): ')
	# While loop ensures that input is a positive integer 
    while isNumber(num) == 'NaN' or math.isnan(isNumber(num)) == True or int(num) < 0:
        if num == 'x':
            print()
            print('=== Program ended === ')
            print()
            exit()
        else:
            print('   Not a positive integer!')
            # print()
            num = input('Enter an integer (x to exit): ')

    if int(num) == 0 or int(num) == 1:
        print('   ' + num + ' NOT PRIME')

    elif int(num) == 2 or int(num) == 3:
        print('   ' + num + ' IS PRIME' )

    else:
        i = 2
        while i < int(num):
            if int(num) % i == 0:
                print('   ' + num + ' NOT PRIME')
                break
            i = i + 1
            if int(num) == i + 1:
                print('   ' + num + ' IS PRIME')