import os, math, op, dict


# 01: Clear screen
def cls():
    os.system('cls' if os.name=='nt' else 'clear')


	
# 02: Select or change language
def language():
    lang = input('Select (en|de): ')
    # If input is not en/de, repeat lang prompt
    while lang.lower() != 'en' and lang.lower() != 'de':
        print('ERROR: ' + lang)
        lang = input('Select (en|de): ')
    # Return lowercased en|de to the function-call
    return lang.lower()

	

# 03: Number test
# Test by trying a float-conversion of user-input
# If float() works, return the float
# If float() fails, return the string 'Nan' 
def isNumber(userArg):
    while True:
        try:
            return float(userArg)
            break
        except ValueError: # If float() fails a "ValueError" exception is raised (right type but inappropriate value)
            return 'NaN'


# 04: Two-argument operations
# Accept user inputs for two arguments, pass the two arguments to the user-requested operation
def twoArgFx(operation, lang): # user-requested operation is received in the operation param
    
	# Argument 1
    a = input(dict.enterArg1[lang])
	# While loop to enforce user-input for arg 'a' is a number
    while isNumber(a) == 'NaN' or math.isnan(isNumber(a)) == True:
        if ',' in a:
            print(dict.CommaError[lang])
        else:
            print(dict.NanError[lang])
        a = input(dict.enterArg1[lang])
    
	#Argument 2
    b = input(dict.enterArg2[lang])
	# While loop to enforce user-input for arg 'a' is a number
    while isNumber(b) == 'NaN' or math.isnan(isNumber(b)) == True:
        if ',' in b:
            print(dict.CommaError[lang])
        else:
            print(dict.NanError[lang])
        b = input(dict.enterArg2[lang])
    
	# Goto user-specified operation
    if operation == 'add':
	    op.addition(a,b)
    if operation == 'sub':
        op.subtraction(a,b)
    if operation == 'mul':
        op.multiplication(a,b)
    if operation == 'div':
        op.division(a,b)
