import fx, dict # Modules containing general functions (fx) and string tables (dict)


# CLI display starts now
fx.cls() # Clear screen then show intro text
print()
print('CalcWizard by Alexander Abraham')
print('===============================')
print()
print('In what language do you want to run this program?')
print('In welcher Sprache möchten Sie dieses Program ausführen?')
print()

# Set val of lang variable to returnval of function fx.language()
# lang will therefore have a value of either 'en' or 'de' 
lang = fx.language()


# Initialize the operation variable with a dummy value. This is so that 
# the next While loop evaluates to True and its While clause starts to execute
operation = 'start'


# While loop to display available operations, accept user-selection, and process operations
# While loop exited when user requests the end op or no more op
while operation != "end" and operation != "n":
    print()    
    print('==========================================================================')
    print()
    print()
	# Desc of available operations: strings in dict module
    print(dict.availOps[lang])
    print(dict.availOpsUL[lang])
    print()
    print(dict.strLan[lang])
    print(dict.strAdd[lang])
    print(dict.strSub[lang])
    print(dict.strMul[lang])
    print(dict.strDiv[lang])
    print(dict.strEnd[lang])
    print()
    print(dict.opList[lang]) # List of keywords to select operation
    operation = input(dict.selectOp[lang]) # User input is stored in var named operation
    print()

	
    # Ensure a valid keyword is selected
    # ==================================
    # While condition stays True if user-selection is not a valid keyword 
    while operation != 'lan' and operation != 'add' and operation != 'sub' and operation != 'mul' and operation != 'div' and operation != 'end':
        # While clause prompts for correct keyword
        print(dict.selectOpError[lang], operation)
        print(dict.opList2[lang])
        operation = input(dict.selectOp[lang]) # User input to operation
        print()

    # Process operations
    # ==================	
    # If condition is True if user-selection is add, sub, mul, div; it filters out 'end' and 'lan'
    if operation != 'end' and operation != 'lan':
        fx.twoArgFx(operation, lang) # Call the two-arg function, supplying two params, for add, sub, mul, div
        print()
        operation = input (dict.newOpQry[lang]).lower() # Ask if another operation is needed
        while operation != 'y' and operation != 'n': # While loop to enforce y|n as user-input
            print(dict.newOpSelectError[lang], operation)
            operation = input(dict.newOpQry2[lang]).lower()		
        print()
    # Elif for a user-selection of 'lan', defines processing for 'lan' operation
    elif operation == 'lan':
        lang = fx.language() # Call the fx-language() function, reset the value of lang variable


# While loop is exited if the operation variable contains 'end' or 'n'
print(dict.progEnd[lang])
print()
input('Press ENTER to exit')
exit()