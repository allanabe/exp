
# main.py: Listing of operations
availOps = { "en": "Available Operations", "de": "Verfügbare Operationen" }
availOpsUL = { "en": "====================", "de": "======================" }
strLan = { "en": "lan = Change UI language", "de": "lan = UI-Sprache ändern" }
strAdd = { "en": "add = Addition (a+b)", "de": "add = Addition (a+b)" }
strSub = { "en": "sub = Subtraction (a-b)", "de": "sub = Subtraktion (a-b)" }
strMul = { "en": "mul = Multiplication (a*b)", "de": "mul = Multiplikation (a*b)" }
strDiv = { "en": "div = Division (a/b)", "de": "div = Division (a/b)" }
strEnd = { "en": "end = End program", "de": "end = Programm beenden" }

# main.py: Select an operation
opList = { "en": "Select an operation: [lan|add|sub|mul|div|end]", "de": "Wählen Sie eine Operation aus: [lan|add|sub|mul|div|end]" }
selectOp = { "en": "Which operation? ", "de": "Welche Operation? " }
selectOpError = { "en": "Incorrect keyword:", "de": "Eingabe-Fehler:" }
opList2 = { "en": "Select one of the following: [lan|add|sub|mul|div|end]", "de": "Wählen Sie einer der folgenden Optionen aus: [lan|add|sub|mul|div|end]" }
newOpQry = { "en": "Another operation (Y/N)? ", "de": "Noch eine Operation (Y/N)? " }
newOpSelectError = { "en": "Incorrect input:", "de": "Eingabe-Fehler:" }
newOpQry2 = { "en": "Another operation (Y/N)? Select either Y or N: ", "de": "Noch eine Operation (Y/N)? Wählen Sie entweder Y or N aus: " }

# main.py: End program  
progEnd = { "en": "== Program ended ==", "de": "== Programm beendet ==" }

# function.py
enterArg1 = { "en": "Enter the first argument: ", "de": "Geben Sie das erste Argument ein: " }
enterArg2 = { "en": "Enter the second argument: ", "de": "Geben Sie das zweite Argument ein: " }
CommaError = { "en": "You entered a decimal comma. Enter a decimal point instead (and no other punctuation)!", "de": "Sie haben eine Dezimalkomma eingegeben. Geben Sie stattdessen einen Dezimalpunkt ein (und keine andere Zeichensetzung)!" }
NanError = { "en": "You did not enter a number. Try again!", "de": "Eingabe ist keine Zahl. Versuchen Sie es erneut!" }
