import os

def cls():
    os.system('cls' if os.name=='nt' else 'clear')

cls()
print('')
print('The Gauss Summation Trick')
print('=========================')
print('')
total = 0
n = int(input("Enter the number up to which you wish to sum: "))
print('')
print('NextNum + Total = Total')
for num in range(1, n+1):
    print(str(num) + '+' + str(total)  + '=' + str(total+num))
    total = total + num
print('Sum of all numbers from 1 to ' + str(n) + ' = ' + str(total))
print('')
print('Using the formula [(n+1)/2]*n, where n=' + str(n) + ', we get: ')
print('[(' + str(n) + '+1)/2]*' + str(n) + ' =')
print('[' + str((n+ 1)/2) + ']*' + str(n) + ' =')
print(str(int((n+1)/2*n)))
