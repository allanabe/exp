#Addition operation
def addition(a,b):
    add = round((float(a)+float(b)), 10)
    print (str(a) + ' + ' + str(b) + ' = ' + str(add))


# Subtraction operation
def subtraction(a,b):
    sub = round((float(a)-float(b)), 10)
    print (str(a) + ' - ' + str(b) + ' = ' + str(sub))


# Multiplication operation
def multiplication(a,b):
    mul = round((float(a)*float(b)), 10)
    print (str(a) + ' * ' + str(b) + ' = ' + str(mul))


# Division operation
def division(a,b):
    div = round((float(a) / float(b)), 10)
    print (str(a) + ' div ' + str(b) + ' = ' + str(div))
